import { getAllPosts } from "@src/repository/postRepository";
import { Post } from "@src/types/post";
import { useState } from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";
import PostListItem from "@src/components/PostListItem";

export default function Page() {
  const [posts, setPosts] = useState<Post[]>(getAllPosts());

  return (
    <View style={styles.container}>
      <View style={styles.main}>
        <FlatList data={posts}
          contentContainerStyle={{gap: 10}}
          renderItem={({item}) => (
            <PostListItem post={item} />
          )} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
  },

  main: {
    flex: 1,
    justifyContent: "center",
    maxWidth: 960,
    marginHorizontal: "auto",
  },

  title: {
    fontSize: 64,
    fontWeight: "bold",
  },

  subtitle: {
    fontSize: 36,
    color: "#38434D",
  },
});
