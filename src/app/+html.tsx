import { PropsWithChildren } from "react";
import Head from 'expo-router/head';
import { ScrollViewStyleReset } from 'expo-router/html';

export default function Root({ children }: PropsWithChildren) {
    return (
        <html lang="en">
        <head>
            <meta charSet="utf-8" />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

            
            <ScrollViewStyleReset />
        </head>
        <Head>
            <title>My Blog Website</title>
        </Head>
        <body>
            {children}
        </body>
        </html>
    )
}