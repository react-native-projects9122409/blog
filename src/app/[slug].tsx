import { View, Text, StyleSheet, ScrollView, Image } from 'react-native'
import React, { useState } from 'react'
import { useLocalSearchParams } from 'expo-router'
import Head from 'expo-router/head';
import Markdown from 'react-native-markdown-display';

import { Post } from '@src/types/post';
import { getAllPosts, getPost } from '@src/repository/postRepository';
import * as config from '@src/config';

export async function generateStaticParams(): Promise<Record<string, string>[]> {
  const posts = getAllPosts();
  
  return posts.map((post) => ({
    slug: post.slug
  }));
}

const PostDetailsPage = () => {
  const {slug} = useLocalSearchParams();
  const [post, setPost] = useState<Post>(getPost(slug as string));

  if (!post) {
    return (
        <Text>Post not found</Text>
    )
  }

  return (
    <>
      <Head>
        <title>{post.title}</title>
      </Head>
      <ScrollView 
        style={styles.postContainer}
        contentContainerStyle={{
            maxWidth: 960,
            width: '100%',
            marginHorizontal: 'auto',
            padding: 20,
            backgroundColor: 'white',
        }}
      >
        <Text style={styles.titleText}>{post.title}</Text>
        <Image source={{ uri: `${config.ORIGIN}/thumbnails/${post.thumbnail}`}} style={styles.postImage} alt={post.title} />
        <Markdown>{post.content}</Markdown>
      </ScrollView>
    </>
  )
}

const styles = StyleSheet.create({
    postContainer: {
        flex: 1,
    },

    titleText: {
        marginBottom: 20,
        fontSize: 30,
        fontWeight: '700',
    },

    postImage: {
      width: '100%',
      aspectRatio: '16/9',
    }
})

export default PostDetailsPage