import posts from "@src/content";

export function getAllPosts() {
    return Object.values(posts);
}

export function getPost(slug: string) {
    return posts[slug];
}