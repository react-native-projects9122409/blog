import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import { Post } from '@src/types/post'
import { Link } from 'expo-router'

type PostLiteItemParams = {
    post: Post
}

const PostListItem = ({post}: PostLiteItemParams) => {
  return (
    <View>
      <Link href={`/${post.slug}`}>{post.title}</Link>
    </View>
  )
}

const styles = StyleSheet.create({

});

export default PostListItem