import { Post } from "../types/post";

const post: Post = {
    slug: 'post3',
    date: '202405-12T13:27:28Z',
    title: 'This is a new post',
    description: 'The newest post in this blog.',
    thumbnail: 'post3.png',
    content: `# The Newest Post
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac semper risus, id hendrerit turpis. Nullam pellentesque fringilla eleifend. Sed maximus ultrices nibh et laoreet. Etiam sed viverra ante. Fusce iaculis a lacus vitae facilisis. Nunc vel sapien erat.

Here are some examples:
- example 1
- example 2
- example 3

And also some order list:
1. list item 1
2. list item 2
3. list item 3
    `
};

export default post;