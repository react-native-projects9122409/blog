import { Post } from "../types/post";

const post: Post = {
    slug: 'post2',
    date: '202402-28T13:27:28Z',
    title: 'This is my second post',
    description: 'This is my second post',
    thumbnail: 'post2.png',
    content: `
# Tamen geniti potes

## Oculis fertilis sustulit Lebinthos referebam redeat tuetur

*Lorem markdownum* refer si forte elige, Alce sed; haec videt genas seu! Sub
victricia velat, **nubibus urnam**, crescendo hanc Heliades parat dixerunt,
[orbem velut](http://sinunt.com/utrumque). Latet coniunx sic animam parte dumque
thalamosque fallor parat **non**.

    imap_character_graphic(ivr(1, file_box_fsb, platform_gps_sampling),
            ddl.clean.responsiveTitle(camera_thread_cpm, registry(
            pptp_topology)), footerTorrent);
    if (bluetoothWiki * keyboard + nui) {
        alert_midi(readerSpriteCertificate);
        bloatware(biometrics, 4, virusNorthbridgeInsertion);
    } else {
        bankruptcy_graphics.variable_username(infotainment, textCpm,
                rom_primary);
        modem.mini_schema_ethernet *= smb_icon_key;
    }
    serpManet(18);

## Cuius carinae

Rogos nec *Marte habuisse olorinis* biceps disceditis haerentes erexit, conubia
est verbere. Nostri **inposuit** nescioquam dolebo iuvenis tibi fronte aquarum
at lecta, hic ut senem notas, lac et dubites illa? Cervina equorum o miserata
specta **et manu** patent veni tuli. Non binas validosque euntem. Vecordia quas
sua obviaque saucia fremit!

## Altera stagno

Iniqui e animas. Tum modo mollescat quoque tum, conata rediit trucis Sidonide.

    var web_fpu = vaporware.hit_firmware(-3, native_heuristic_lpi(
            mirroredCadMethod, target_time * development), dual(1, -1));
    drive_webmaster(drive + listserv_meta - backside_rdram_bittorrent, header);
    spreadsheet(kvm_memory_google, bespoke / load_horizontal_monochrome -
            disk_packet);
    if (ppiCssBcc + 1 * bar + 60) {
        ppm = post;
        formula -= 15;
        adwareSambaOffice = pciPlainCamera(3);
    } else {
        nullUnicode(1 + macCompact, markupModelReadme);
    }

## Unde et nunc quae subigebat inque sine

Quam passos quae decurrere Iunonius a! *Obstaret qua* consuetas vitta **Lunae
despectat lactantiaque** arce adspexit domos est. Quoque ruricolae in iura
memorique premeret. Sua pullo Andros **te fecit**.

## Telis rigido reluxit remittit iras

Doluit ab Minervae non [denique enim](http://www.munerisora.com/ettori),
sollicitat frigidus occupat. Laqueos origine se dilecta eius conpellat rigidis
quod iam **macies ausis pater** Tyrrhenus, movetur belli, erat.

Sedit albet: hic maxima capillis, frondis auctor. Deserit prospexerat praereptae
femur rorantesque minus concita, Hister surrexit, mediis, artisque meas. Deus
Cinyreius parens! Nec gemini timor, cornibus Iovis quae ducit, ingemuitque.

Menelae res, florem me diro tibi quibus verba palmis furtim, apertum virgo.
Syringa ruere, diu *rogavi victor* obstruat redeunt sensit inhospita fallit,
vigilans annos suos cernit,
[una](http://quidemformosissimus.org/multis-casus.aspx). Hanc auguror echidnis
aliter et **virum contulit tamen** ne nostro ut, ad domusque simile.
    `
};


export default post;