import { Post } from "../types/post";

const post: Post = {
    slug: 'post1',
    date: '202402-28T13:27:28Z',
    title: 'Post 1',
    description: 'Post 1',
    thumbnail: 'post1.png',
    content: `# Agit nec

## Maximus pennae aequali tabuerit gregesque qualis nitorem

Lorem markdownum! In quod hostem sinu monte vomit, et signorum Thaumantidos
Iphi: est. Ripas pariter quae. Adspicit et sociis dixit, non dixi capta et
discedite Hector, infelix Rhodanumque?

1. Qui sis sed ad decerpsit
2. Inopi pectore
3. Rura vertice avidum circumsona victam multa
4. Diversa horruit oro tulit stratis humo profanus

Colonos sed atque illis Iunonis frontes vidisse verba, est sunt hebes maeret
grandaevique et tamen ibit lata *gurgite trunco*. Veniam potentia cucurri
vallibus statione quondam ergo duros stirpe marcentia Midae equarum, [Lycopen
ensis](http://panopeusquemodo.io/incestomeo.html) sacrorum collo omni puerique
etiam. Profuit videt, *qui omnia et* caelum. Comitique avia condas venefica
**avi decent illis** posset, dura Sigea legebat simulacra a foedaque sed
**eiectas pectus**, aere. Bene silvis fuit; vel neque et diffusum Giganteo sed
inritaturque candor Cnosia et.

## Est tellus super quicquid restitit Iunone

Campo vetuit panda et et fuit morsibus et corpore aurea conticuere, sed Lucina
tua. Ingemuit exserere **crinem Italicis** gestu. Idmoniae tuus
[nostra](http://est.net/), hanc equorum Icare, inplevit inhibere mihi tua tibi.
Per languida conspexit Erysicthone Thoactes genitor.

- Quod magis silvae
- Colla terra in terga
- Irae placidis obiectat postquam operum fecit incubat
- Sacer silva cum moriorque gemitus

Se mutata fraga qui timoris Ceycis lucum illo caruisse quae, arces vides
spectabilis. Ut et igni huic illa et coctilibus et pedis sinistro postquam
cognorat fugit. O cadet, his aut iuro iubete aquis: aurigam lacrimas, ante
gurgite mille suae nec referat. Seque **dedit silvas**, totiens undas pudet
Cybeleia feruntur decorum: cor antiquas exsiluit, [regni
caesariem](http://aurum.io/quamdies).

- Post est idem illa noctes tuos strenua
- Tamen sua dumque flamma iter neque undis
- Pignora humumque suas lumen pestifero sumit saetis
- Cadunt quies
- Somnum iam tristis terras placidissime vero

Cornua raptu **mors** deae, aula est, dare gemini, sinunt Apollo, ire *est
aera*. Amori similesque arceor solebat una *tangit* non, vixisse quod, Nedymnum.
Porrigitur deferre cecidere inploravere vidit Procne tu vir vidit; in quarum
laeter et curvarique hoc. Ilia nec tu fraterno meminisse vero est erat morsusque
precor.`
};

export default post;